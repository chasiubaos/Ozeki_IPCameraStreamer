﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using AForge.Video.DirectShow;
using Ozeki.Media;
using Ozeki.Camera;

using Facebook;
using MediaToolkit;

using SportGameRecorder.Objects;
using SportGameRecorder.Forms;
using Newtonsoft.Json.Linq;

namespace SportGameRecorder
{
    public partial class RecorderForm : Form
    {
        // Constants
        private static int SCOREBOARD_Y_COORDINATE = 480;
        private static String FONT = "Arial";

        // List of Tables
        List<Table> tables;

        // Media components
        private DrawingImageProvider _provider;
        private MediaConnector _connector;
        private IIPCamera _ipCamera;
        private Speaker _speaker;
        private VideoViewerWF _videoViewerWf;
        private MPEG4Recorder _mpeg4Recorder;
        private VideoStreamHandler _streamHandler;

        // Overlays
        private ImageMask _imageMask;
        private bool _imageMaskRefreshing = false;
        Image _originalImage = Image.FromFile("..//..//scoreboard.bmp");

        // Scores
        private volatile int _p1Score = 0;
        private volatile int _p2Score = 0;
        private volatile int _p1NameXLocation;
        private volatile int _p2NameXLocation;

        // Recording
        private bool isRecording = false;

        // Streaming
        private FacebookClient _fbClient = new FacebookClient();

        public RecorderForm()
        {
            InitializeComponent();

            _connector = new MediaConnector();
            _provider = new DrawingImageProvider();
            _speaker = Speaker.GetDefaultDevice();

            _p1NameXLocation = tbP1NamePosition.Value;
            _p2NameXLocation = tbP2NamePosition.Value;

            SetVideoViewer();

            tables = new List<Table>();
            if (File.Exists("./tables.json"))
            {
                String jsonText = File.ReadAllText("./tables.json");
                JArray jsonArray = JArray.Parse(jsonText);

                tables = jsonArray.ToObject<List<Table>>();
            }
            PopulateTableList();
        }

        private void PopulateTableList()
        {
            lstTables.Items.Clear();
            foreach (Table table in tables)
            {
                lstTables.Items.Add("Table " + table.id.ToString());
            }
        }

        private void SetVideoViewer()
        {
            _imageMask = new ImageMask();

            _videoViewerWf = new VideoViewerWF();
            _videoViewerWf.Size = new Size(CameraBox.Width, CameraBox.Height);
            _videoViewerWf.BackColor = Color.Black;
            _videoViewerWf.TabStop = false;
            _videoViewerWf.Location = new Point(10, 20);
            _videoViewerWf.Name = "VideoViewer";

            CameraBox.Controls.Add(_videoViewerWf);
        }

        private void _camera_StateChanged(object sender, CameraStateEventArgs e)
        {
            new Task(() =>
            {
                txtStatus.Text = e.State.ToString();
            }).Start();
        }

        private void _camera_ErrorOccurred(object sender, CameraErrorEventArgs e)
        {
            new Task(() => txtStatus.Text = e.Error.ToString()).Start();
        }

        private void btnGetIPCameras_Click(object sender, EventArgs e)
        {
            lstCameras.Items.Clear(); 
            IPCameraFactory.DeviceDiscovered -= IPCameraFactory_DeviceDiscovered;

            GetIpCameras();
        }

        private void GetIpCameras()
        {
            IPCameraFactory.DeviceDiscovered += IPCameraFactory_DeviceDiscovered;
            IPCameraFactory.DiscoverDevices();
        }

        private void IPCameraFactory_DeviceDiscovered(object sender, DiscoveryEventArgs e)
        {
            new Task(() => lstCameras.Items.Add(e.Device.Host + ":" + e.Device.Port)).Start();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (_ipCamera != null)
            {
                _ipCamera.Disconnect();
            }
            Clear();

            // Need to add capability to have input passwords or remove passwords from cameras
            String host = (String)lstCameras.SelectedItem;
            _ipCamera = IPCameraFactory.GetCamera(host, "admin", "88888888");

            // Connects the video channel -> the image mask -> the score overlay -> provider to show it to peoples
            _connector.Connect(_ipCamera.VideoChannel, _imageMask);
            _connector.Connect(_imageMask, _provider);
            _connector.Connect(_imageMask, _streamHandler);

            // Conencts the audio channel -> default speaker
            // _connector.Connect(_ipCamera.AudioChannel, _speaker);

            // DEBUG CODE: LET'S YOU SELECT WHAT IMAGE TO USE AS MASK
            // For now we are going to let us choose what overlay we want
            //var filename = "";
            //var ofd = new OpenFileDialog();
            //var dr = ofd.ShowDialog();

            //if (dr == DialogResult.OK)
            //{
            //    filename = ofd.FileName;
            //}

            //if (filename != "")
            //{
            //    _imageMask.Image = Image.FromFile(filename);
            //    _imageMask.MaskOption = MaskOption.Foreground;
            //}

            _ipCamera.CameraStateChanged += _camera_StateChanged;
            _ipCamera.CameraErrorOccurred += _camera_ErrorOccurred;

            _videoViewerWf.SetImageProvider(_provider);

            _imageMask.Image = CreateImageMask();
            _imageMask.MaskOption = MaskOption.Foreground;
            if (cboxScoreboard.Checked)
                _imageMask.Start();

            _ipCamera.Start();
            _speaker.Start();
            _videoViewerWf.Start();
            timer.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (_ipCamera != null)
            {
                _videoViewerWf.Stop();
                _ipCamera.Stop();
                _speaker.Stop();
                timer.Stop();

                _ipCamera = null;
            }
        }

        private Image CreateImageMask()
        {
            // The following are the points for:
            //      1. Player 1's Name
            //      2. Player 2's Name
            //      3. Player 1's Score
            //      4. Player 2's Score
            //      5. Race to
            // Need to add functionality to shift X axis of the names so that it can fit nicely
            Point p1NameLocation = new Point(_p1NameXLocation, SCOREBOARD_Y_COORDINATE);
            Point p2NameLocation = new Point(_p2NameXLocation, SCOREBOARD_Y_COORDINATE);
            Point p1ScoreLocation = new Point(400, SCOREBOARD_Y_COORDINATE);
            Point p2ScoreLocation = new Point(690, SCOREBOARD_Y_COORDINATE);
            Point raceToLocation = new Point(480, SCOREBOARD_Y_COORDINATE);

            Image image = (Image) _originalImage.Clone();

            using (Graphics graphic = Graphics.FromImage(image))
            {
                graphic.DrawString(tbP1Name.Text, new Font(new FontFamily(FONT), 24, FontStyle.Bold), SystemBrushes.WindowText, p1NameLocation);
                graphic.DrawString(tbP2Name.Text, new Font(new FontFamily(FONT), 24, FontStyle.Bold), SystemBrushes.WindowText, p2NameLocation);
                graphic.DrawString(_p1Score.ToString(), new Font(new FontFamily(FONT), 24, FontStyle.Bold), SystemBrushes.WindowText, p1ScoreLocation);
                graphic.DrawString(_p2Score.ToString(), new Font(new FontFamily(FONT), 24, FontStyle.Bold), SystemBrushes.WindowText, p2ScoreLocation);

                graphic.DrawString("Race to " + nudRaceTo.Value.ToString(), new Font(new FontFamily(FONT), 24, FontStyle.Regular), new SolidBrush(Color.LightSlateGray), raceToLocation);
            }

            return image;
        }

        private void Clear()
        {
            new Task(() =>
            {
                txtStatus.Text = "";
                txtError.Text = "";
            }).Start();
        }

        private void btnMoveCamera_ButtonDown(object sender, EventArgs e)
        {
            if (_ipCamera != null && _ipCamera.CameraMovement.IsPTZSupported)
            {
                string buttonClicked = ((Button)sender).Text;

                switch (buttonClicked)
                {
                    case "Up":
                        _ipCamera.CameraMovement.ContinuousMove(MoveDirection.Up);
                        break;
                    case "Right":
                        _ipCamera.CameraMovement.ContinuousMove(MoveDirection.Right);
                        break;
                    case "Down":
                        _ipCamera.CameraMovement.ContinuousMove(MoveDirection.Down);
                        break;
                    case "Left":
                        _ipCamera.CameraMovement.ContinuousMove(MoveDirection.Left);
                        break;
                }
            }
        }

        private void btnMoveCamera_ButtonUp(object sender, EventArgs e)
        {
            if (_ipCamera != null && _ipCamera.CameraMovement.IsPTZSupported)
            {
                _ipCamera.CameraMovement.StopMovement();
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            new Task(() =>
            {
                if (_ipCamera != null && _ipCamera.CurrentStream.VideoEncoding != null)
                {
                    txtFps.Text = _ipCamera.CurrentStream.VideoEncoding.FrameRate.ToString();
                    txtBitRate.Text = _ipCamera.CurrentStream.VideoEncoding.BitRate.ToString();
                }
            }).Start();
        }

        private void btnRecord_Click(object sender, EventArgs e)
        {
            string fileName;

            if (!isRecording)
            {
                if (String.IsNullOrEmpty(txtFileName.Text))
                {
                    fileName = "Default.mp4";
                }
                else
                {
                    fileName = txtFileName.Text + ".mp4";
                }

                _mpeg4Recorder = new MPEG4Recorder(fileName);
                _mpeg4Recorder.MultiplexFinished += _mpeg4Recorder_MultiplexFinished;

                _connector.Connect(_imageMask, _mpeg4Recorder.VideoRecorder);
                _connector.Connect(_ipCamera.AudioChannel, _mpeg4Recorder.AudioRecorder);

                isRecording = true;

                btnRecord.Text = "Stop";
            }
            else
            {
                if (_mpeg4Recorder != null)
                {
                    _mpeg4Recorder.Multiplex();

                    _connector.Disconnect(_imageMask, _mpeg4Recorder.VideoRecorder);
                    _connector.Disconnect(_ipCamera.AudioChannel, _mpeg4Recorder.AudioRecorder);

                    isRecording = false;

                    btnRecord.Text = "Record";
                }
            }
        }

        private void _mpeg4Recorder_MultiplexFinished(object sender, VoIPEventArgs<bool> e)
        {
            _connector.Disconnect(_imageMask, _mpeg4Recorder.VideoRecorder);
            _connector.Disconnect(_ipCamera.AudioChannel, _mpeg4Recorder.AudioRecorder);

            _mpeg4Recorder.Dispose();
        }

        private void p1Plus_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("[DEBUG] P1Plus pressed...");
            if (!_imageMaskRefreshing)
            {
                Debug.WriteLine("[DEBUG] Player 1 scored a point...");
                _p1Score++;
                RefreshImageMask();
            }
        }

        private void p1Minus_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("[DEBUG] P1Minus pressed...");
            if (!_imageMaskRefreshing && _p1Score > 0)
            {
                Debug.WriteLine("[DEBUG] Player 1 lost a point...");
                _p1Score--;
                RefreshImageMask();
            }
        }

        private void p2Plus_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("[DEBUG] P2Plus pressed...");
            if (!_imageMaskRefreshing)
            {
                Debug.WriteLine("[DEBUG] Player 2 scored a point...");
                _p2Score++;
                RefreshImageMask();
            }
        }

        private void p2Minus_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("[DEBUG] P2Minus pressed...");
            if (!_imageMaskRefreshing && _p2Score > 0)
            {
                Debug.WriteLine("[DEBUG] Player 2 lost a point...");
                _p2Score--;
                RefreshImageMask();
            }
        }

        private async void RefreshImageMask()
        {
            Debug.WriteLine("[DEBUG] Refreshing Image Mask...");
            _imageMaskRefreshing = true;
            await Task.Run(() =>
            {
                Debug.WriteLine("[DEBUG] Starting refresh task...");
                _imageMask.Stop();
                _imageMask.Image = CreateImageMask();
                _imageMask.Start();

                _imageMaskRefreshing = false;
                Debug.WriteLine("[DEBUG] Mask refresh completed...");
            });
        }

        private void cboxScoreboard_CheckedChanged(object sender, EventArgs e)
        {
            if (cboxScoreboard.Checked && !_imageMask.IsRunning)
            {
                _imageMask.Start();
            }
            else if (!cboxScoreboard.Checked && _imageMask.IsRunning)
            {
                _imageMask.Stop();
            }
        }

        private void tbPlayerName_Leave(object sender, EventArgs e)
        {
            if (!_imageMaskRefreshing)
            {
                RefreshImageMask();
            }
        }

        private void tbPlayerName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 && !_imageMaskRefreshing)
                RefreshImageMask();
        }

        private void nudRaceTo_ValueChanged(object sender, EventArgs e)
        {
            if (!_imageMaskRefreshing)
            {
                RefreshImageMask();
            }
        }

        private void tbNamePosition_MouseUp(object sender, MouseEventArgs e)
        {
            _p1NameXLocation = tbP1NamePosition.Value;
            _p2NameXLocation = tbP2NamePosition.Value;

            if (!_imageMaskRefreshing)
            {
                RefreshImageMask();
            }
        }

        private void btnFacebook_Click(object sender, EventArgs e)
        {
            FacebookLoginForm dialog = new FacebookLoginForm(_fbClient);
            var result = dialog.ShowDialog();
            
            if (result == DialogResult.OK)
            {
                _fbClient = dialog._fbClient;
                Debug.WriteLine("[DEBUG] Signed in to FACEBOOK. Access Token: " + _fbClient.AccessToken + "...");
            }
        }

        private void btnStream_Click(object sender, EventArgs e)
        {
            if (_ipCamera == null)
            {
                return;
            }
        }

        private void addTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            EditTableForm dialog = new EditTableForm(tables);
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                tables = dialog.currentTables;
                PopulateTableList();
                Debug.WriteLine("[DEBUG] New table dialog opened...");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lstTables.SelectedIndex >= 0)
            {
                String selection = lstTables.SelectedItem.ToString();
                Int32.TryParse(selection.Split(' ')[1], out int selectionIndex);

                EditTableForm dialog = new EditTableForm(tables, selectionIndex);
                var result = dialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    tables = dialog.currentTables;
                    PopulateTableList();
                    Debug.WriteLine("[DEBUG] Edit table dialog opened...");
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lstTables.SelectedIndex >= 0)
            {
                var result = MessageBox.Show("Are you sure you want to delete " + lstTables.SelectedItem.ToString() + "?",
                    "Delete Table",
                    MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    String selection = lstTables.SelectedItem.ToString();
                    Int32.TryParse(selection.Split(' ')[1], out int selectionIndex);

                    int index = tables.FindIndex(t => t.id == selectionIndex);
                    tables.RemoveAt(index);

                    JArray json = (JArray)JToken.FromObject(tables);
                     
                    File.WriteAllText("./tables.json", json.ToString());

                    PopulateTableList();
                    Debug.WriteLine("[DEBUG] " + selection.ToString() + " deleted...");
                }
            }
        }
    }
}
