﻿namespace SportGameRecorder
{
    partial class RecorderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lstCameras = new System.Windows.Forms.ListBox();
            this.btnGetIPCameras = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.CameraBox = new System.Windows.Forms.GroupBox();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.txtFps = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBitRate = new System.Windows.Forms.TextBox();
            this.txtError = new System.Windows.Forms.TextBox();
            this.btnRecord = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbP2Name = new System.Windows.Forms.TextBox();
            this.tbP1Name = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cboxScoreboard = new System.Windows.Forms.CheckBox();
            this.pnlPlayer1 = new System.Windows.Forms.Panel();
            this.tbP1NamePosition = new System.Windows.Forms.TrackBar();
            this.btnP1Plus = new System.Windows.Forms.Button();
            this.btnP1Minus = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbP2NamePosition = new System.Windows.Forms.TrackBar();
            this.btnP2Plus = new System.Windows.Forms.Button();
            this.btnP2Minus = new System.Windows.Forms.Button();
            this.nudRaceTo = new System.Windows.Forms.NumericUpDown();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnFacebook = new System.Windows.Forms.Button();
            this.btnStream = new System.Windows.Forms.Button();
            this.lstTables = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pnlPlayer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbP1NamePosition)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbP2NamePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRaceTo)).BeginInit();
            this.SuspendLayout();
            // 
            // lstCameras
            // 
            this.lstCameras.FormattingEnabled = true;
            this.lstCameras.Location = new System.Drawing.Point(12, 162);
            this.lstCameras.Name = "lstCameras";
            this.lstCameras.Size = new System.Drawing.Size(219, 108);
            this.lstCameras.TabIndex = 3;
            // 
            // btnGetIPCameras
            // 
            this.btnGetIPCameras.Location = new System.Drawing.Point(12, 276);
            this.btnGetIPCameras.Name = "btnGetIPCameras";
            this.btnGetIPCameras.Size = new System.Drawing.Size(219, 23);
            this.btnGetIPCameras.TabIndex = 4;
            this.btnGetIPCameras.Text = "Get Cameras";
            this.btnGetIPCameras.UseVisualStyleBackColor = true;
            this.btnGetIPCameras.Click += new System.EventHandler(this.btnGetIPCameras_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 305);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(219, 23);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // CameraBox
            // 
            this.CameraBox.Location = new System.Drawing.Point(237, 6);
            this.CameraBox.Name = "CameraBox";
            this.CameraBox.Size = new System.Drawing.Size(818, 511);
            this.CameraBox.TabIndex = 6;
            this.CameraBox.TabStop = false;
            this.CameraBox.Text = "Video Stream";
            // 
            // btnUp
            // 
            this.btnUp.Location = new System.Drawing.Point(76, 470);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(75, 23);
            this.btnUp.TabIndex = 9;
            this.btnUp.Text = "Up";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonDown);
            this.btnUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonUp);
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(76, 528);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(75, 23);
            this.btnDown.TabIndex = 12;
            this.btnDown.Text = "Down";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonDown);
            this.btnDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonUp);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(119, 499);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(75, 23);
            this.btnRight.TabIndex = 11;
            this.btnRight.Text = "Right";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonDown);
            this.btnRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonUp);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(37, 499);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(75, 23);
            this.btnLeft.TabIndex = 10;
            this.btnLeft.Text = "Left";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonDown);
            this.btnLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveCamera_ButtonUp);
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(131, 366);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(100, 20);
            this.txtStatus.TabIndex = 7;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // txtFps
            // 
            this.txtFps.Location = new System.Drawing.Point(131, 418);
            this.txtFps.Name = "txtFps";
            this.txtFps.Size = new System.Drawing.Size(100, 20);
            this.txtFps.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 421);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "FPS:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 447);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Bit Rate:";
            // 
            // txtBitRate
            // 
            this.txtBitRate.Location = new System.Drawing.Point(131, 444);
            this.txtBitRate.Name = "txtBitRate";
            this.txtBitRate.Size = new System.Drawing.Size(100, 20);
            this.txtBitRate.TabIndex = 14;
            // 
            // txtError
            // 
            this.txtError.Location = new System.Drawing.Point(131, 392);
            this.txtError.Name = "txtError";
            this.txtError.Size = new System.Drawing.Size(100, 20);
            this.txtError.TabIndex = 8;
            // 
            // btnRecord
            // 
            this.btnRecord.Location = new System.Drawing.Point(980, 518);
            this.btnRecord.Name = "btnRecord";
            this.btnRecord.Size = new System.Drawing.Size(75, 23);
            this.btnRecord.TabIndex = 16;
            this.btnRecord.Text = "Record";
            this.btnRecord.UseVisualStyleBackColor = true;
            this.btnRecord.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(803, 520);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(174, 20);
            this.txtFileName.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(729, 523);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Video Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 395);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Errors:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 369);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Camera Status:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Player 2:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Player 1:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1061, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Race to:";
            // 
            // tbP2Name
            // 
            this.tbP2Name.Location = new System.Drawing.Point(113, 3);
            this.tbP2Name.Name = "tbP2Name";
            this.tbP2Name.Size = new System.Drawing.Size(100, 20);
            this.tbP2Name.TabIndex = 21;
            this.tbP2Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPlayerName_KeyPress);
            this.tbP2Name.Leave += new System.EventHandler(this.tbPlayerName_Leave);
            // 
            // tbP1Name
            // 
            this.tbP1Name.Location = new System.Drawing.Point(113, 3);
            this.tbP1Name.Name = "tbP1Name";
            this.tbP1Name.Size = new System.Drawing.Size(100, 20);
            this.tbP1Name.TabIndex = 20;
            this.tbP1Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPlayerName_KeyPress);
            this.tbP1Name.Leave += new System.EventHandler(this.tbPlayerName_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1061, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Scoreboard:";
            // 
            // cboxScoreboard
            // 
            this.cboxScoreboard.AutoSize = true;
            this.cboxScoreboard.Checked = true;
            this.cboxScoreboard.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxScoreboard.Location = new System.Drawing.Point(1265, 17);
            this.cboxScoreboard.Name = "cboxScoreboard";
            this.cboxScoreboard.Size = new System.Drawing.Size(15, 14);
            this.cboxScoreboard.TabIndex = 29;
            this.cboxScoreboard.UseVisualStyleBackColor = true;
            this.cboxScoreboard.CheckedChanged += new System.EventHandler(this.cboxScoreboard_CheckedChanged);
            // 
            // pnlPlayer1
            // 
            this.pnlPlayer1.Controls.Add(this.tbP1NamePosition);
            this.pnlPlayer1.Controls.Add(this.btnP1Plus);
            this.pnlPlayer1.Controls.Add(this.btnP1Minus);
            this.pnlPlayer1.Controls.Add(this.label7);
            this.pnlPlayer1.Controls.Add(this.tbP1Name);
            this.pnlPlayer1.Location = new System.Drawing.Point(1063, 72);
            this.pnlPlayer1.Name = "pnlPlayer1";
            this.pnlPlayer1.Size = new System.Drawing.Size(216, 227);
            this.pnlPlayer1.TabIndex = 30;
            // 
            // tbP1NamePosition
            // 
            this.tbP1NamePosition.Location = new System.Drawing.Point(6, 29);
            this.tbP1NamePosition.Maximum = 350;
            this.tbP1NamePosition.Minimum = 50;
            this.tbP1NamePosition.Name = "tbP1NamePosition";
            this.tbP1NamePosition.Size = new System.Drawing.Size(207, 45);
            this.tbP1NamePosition.TabIndex = 34;
            this.tbP1NamePosition.TickFrequency = 5;
            this.tbP1NamePosition.Value = 150;
            this.tbP1NamePosition.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbNamePosition_MouseUp);
            // 
            // btnP1Plus
            // 
            this.btnP1Plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP1Plus.Location = new System.Drawing.Point(126, 80);
            this.btnP1Plus.Name = "btnP1Plus";
            this.btnP1Plus.Size = new System.Drawing.Size(87, 144);
            this.btnP1Plus.TabIndex = 31;
            this.btnP1Plus.Text = "+";
            this.btnP1Plus.UseVisualStyleBackColor = true;
            this.btnP1Plus.Click += new System.EventHandler(this.p1Plus_Click);
            // 
            // btnP1Minus
            // 
            this.btnP1Minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP1Minus.Location = new System.Drawing.Point(3, 80);
            this.btnP1Minus.Name = "btnP1Minus";
            this.btnP1Minus.Size = new System.Drawing.Size(87, 144);
            this.btnP1Minus.TabIndex = 0;
            this.btnP1Minus.Text = "-";
            this.btnP1Minus.UseVisualStyleBackColor = true;
            this.btnP1Minus.Click += new System.EventHandler(this.p1Minus_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbP2NamePosition);
            this.panel1.Controls.Add(this.btnP2Plus);
            this.panel1.Controls.Add(this.btnP2Minus);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.tbP2Name);
            this.panel1.Location = new System.Drawing.Point(1063, 314);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(216, 227);
            this.panel1.TabIndex = 31;
            // 
            // tbP2NamePosition
            // 
            this.tbP2NamePosition.Location = new System.Drawing.Point(3, 29);
            this.tbP2NamePosition.Maximum = 1000;
            this.tbP2NamePosition.Minimum = 700;
            this.tbP2NamePosition.Name = "tbP2NamePosition";
            this.tbP2NamePosition.Size = new System.Drawing.Size(210, 45);
            this.tbP2NamePosition.TabIndex = 33;
            this.tbP2NamePosition.TickFrequency = 5;
            this.tbP2NamePosition.Value = 750;
            this.tbP2NamePosition.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbNamePosition_MouseUp);
            // 
            // btnP2Plus
            // 
            this.btnP2Plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP2Plus.Location = new System.Drawing.Point(126, 80);
            this.btnP2Plus.Name = "btnP2Plus";
            this.btnP2Plus.Size = new System.Drawing.Size(87, 144);
            this.btnP2Plus.TabIndex = 31;
            this.btnP2Plus.Text = "+";
            this.btnP2Plus.UseVisualStyleBackColor = true;
            this.btnP2Plus.Click += new System.EventHandler(this.p2Plus_Click);
            // 
            // btnP2Minus
            // 
            this.btnP2Minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnP2Minus.Location = new System.Drawing.Point(3, 80);
            this.btnP2Minus.Name = "btnP2Minus";
            this.btnP2Minus.Size = new System.Drawing.Size(87, 144);
            this.btnP2Minus.TabIndex = 0;
            this.btnP2Minus.Text = "-";
            this.btnP2Minus.UseVisualStyleBackColor = true;
            this.btnP2Minus.Click += new System.EventHandler(this.p2Minus_Click);
            // 
            // nudRaceTo
            // 
            this.nudRaceTo.Location = new System.Drawing.Point(1160, 39);
            this.nudRaceTo.Name = "nudRaceTo";
            this.nudRaceTo.Size = new System.Drawing.Size(120, 20);
            this.nudRaceTo.TabIndex = 32;
            this.nudRaceTo.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudRaceTo.ValueChanged += new System.EventHandler(this.nudRaceTo_ValueChanged);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(12, 334);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(219, 23);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnFacebook
            // 
            this.btnFacebook.Location = new System.Drawing.Point(1299, 12);
            this.btnFacebook.Name = "btnFacebook";
            this.btnFacebook.Size = new System.Drawing.Size(97, 49);
            this.btnFacebook.TabIndex = 33;
            this.btnFacebook.Text = "Facebook";
            this.btnFacebook.UseVisualStyleBackColor = true;
            this.btnFacebook.Click += new System.EventHandler(this.btnFacebook_Click);
            // 
            // btnStream
            // 
            this.btnStream.Location = new System.Drawing.Point(1299, 67);
            this.btnStream.Name = "btnStream";
            this.btnStream.Size = new System.Drawing.Size(97, 49);
            this.btnStream.TabIndex = 34;
            this.btnStream.Text = "Stream";
            this.btnStream.UseVisualStyleBackColor = true;
            this.btnStream.Click += new System.EventHandler(this.btnStream_Click);
            // 
            // lstTables
            // 
            this.lstTables.FormattingEnabled = true;
            this.lstTables.Location = new System.Drawing.Point(12, 12);
            this.lstTables.Name = "lstTables";
            this.lstTables.Size = new System.Drawing.Size(219, 108);
            this.lstTables.TabIndex = 37;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(12, 126);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(60, 23);
            this.btnAdd.TabIndex = 38;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(91, 126);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 23);
            this.btnEdit.TabIndex = 39;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(171, 126);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(60, 23);
            this.btnDelete.TabIndex = 40;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // RecorderForm
            // 
            this.ClientSize = new System.Drawing.Size(1408, 561);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lstTables);
            this.Controls.Add(this.btnStream);
            this.Controls.Add(this.btnFacebook);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.nudRaceTo);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.pnlPlayer1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cboxScoreboard);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.btnRecord);
            this.Controls.Add(this.txtBitRate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFps);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.txtError);
            this.Controls.Add(this.btnGetIPCameras);
            this.Controls.Add(this.lstCameras);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.CameraBox);
            this.Name = "RecorderForm";
            this.Text = "Game Stream and Recorder";
            this.TextChanged += new System.EventHandler(this.tbPlayerName_Leave);
            this.pnlPlayer1.ResumeLayout(false);
            this.pnlPlayer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbP1NamePosition)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbP2NamePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRaceTo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lstCameras;
        private System.Windows.Forms.Button btnGetIPCameras;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox CameraBox;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TextBox txtFps;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBitRate;
        private System.Windows.Forms.TextBox txtError;
        private System.Windows.Forms.Button btnRecord;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbP2Name;
        private System.Windows.Forms.TextBox tbP1Name;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox cboxScoreboard;
        private System.Windows.Forms.Panel pnlPlayer1;
        private System.Windows.Forms.Button btnP1Plus;
        private System.Windows.Forms.Button btnP1Minus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnP2Plus;
        private System.Windows.Forms.Button btnP2Minus;
        private System.Windows.Forms.NumericUpDown nudRaceTo;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TrackBar tbP2NamePosition;
        private System.Windows.Forms.TrackBar tbP1NamePosition;
        private System.Windows.Forms.Button btnFacebook;
        private System.Windows.Forms.Button btnStream;
        private System.Windows.Forms.ListBox lstTables;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
    }
}

