﻿namespace SportGameRecorder.Forms
{
    partial class EditTableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTableNum = new System.Windows.Forms.Label();
            this.numTableId = new System.Windows.Forms.NumericUpDown();
            this.lblConnectionString = new System.Windows.Forms.Label();
            this.tbShortCamConnectionString = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbShortCamUsername = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbShortCamPassword = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.bgCameraInformation = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbLongCamPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbLongCamUsername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbLongCamConnectionString = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.vspPreview = new AForge.Controls.VideoSourcePlayer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnCopySS = new System.Windows.Forms.Button();
            this.btnCopyLS = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.lstCameras = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.numTableId)).BeginInit();
            this.bgCameraInformation.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTableNum
            // 
            this.lblTableNum.AutoSize = true;
            this.lblTableNum.Location = new System.Drawing.Point(12, 9);
            this.lblTableNum.Name = "lblTableNum";
            this.lblTableNum.Size = new System.Drawing.Size(77, 13);
            this.lblTableNum.TabIndex = 0;
            this.lblTableNum.Text = "Table Number:";
            // 
            // numTableId
            // 
            this.numTableId.Location = new System.Drawing.Point(106, 9);
            this.numTableId.Name = "numTableId";
            this.numTableId.Size = new System.Drawing.Size(141, 20);
            this.numTableId.TabIndex = 1;
            // 
            // lblConnectionString
            // 
            this.lblConnectionString.AutoSize = true;
            this.lblConnectionString.Location = new System.Drawing.Point(6, 27);
            this.lblConnectionString.Name = "lblConnectionString";
            this.lblConnectionString.Size = new System.Drawing.Size(61, 13);
            this.lblConnectionString.TabIndex = 2;
            this.lblConnectionString.Text = "IP Address:";
            // 
            // tbShortCamConnectionString
            // 
            this.tbShortCamConnectionString.Location = new System.Drawing.Point(94, 24);
            this.tbShortCamConnectionString.Name = "tbShortCamConnectionString";
            this.tbShortCamConnectionString.Size = new System.Drawing.Size(141, 20);
            this.tbShortCamConnectionString.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Username:";
            // 
            // tbShortCamUsername
            // 
            this.tbShortCamUsername.Location = new System.Drawing.Point(94, 50);
            this.tbShortCamUsername.Name = "tbShortCamUsername";
            this.tbShortCamUsername.Size = new System.Drawing.Size(141, 20);
            this.tbShortCamUsername.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Password:";
            // 
            // tbShortCamPassword
            // 
            this.tbShortCamPassword.Location = new System.Drawing.Point(94, 76);
            this.tbShortCamPassword.Name = "tbShortCamPassword";
            this.tbShortCamPassword.PasswordChar = '*';
            this.tbShortCamPassword.Size = new System.Drawing.Size(141, 20);
            this.tbShortCamPassword.TabIndex = 7;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(101, 267);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(182, 267);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bgCameraInformation
            // 
            this.bgCameraInformation.Controls.Add(this.tbShortCamPassword);
            this.bgCameraInformation.Controls.Add(this.label2);
            this.bgCameraInformation.Controls.Add(this.tbShortCamUsername);
            this.bgCameraInformation.Controls.Add(this.label1);
            this.bgCameraInformation.Controls.Add(this.tbShortCamConnectionString);
            this.bgCameraInformation.Controls.Add(this.lblConnectionString);
            this.bgCameraInformation.Location = new System.Drawing.Point(12, 33);
            this.bgCameraInformation.Name = "bgCameraInformation";
            this.bgCameraInformation.Size = new System.Drawing.Size(248, 111);
            this.bgCameraInformation.TabIndex = 10;
            this.bgCameraInformation.TabStop = false;
            this.bgCameraInformation.Text = "Short Side Camera";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbLongCamPassword);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbLongCamUsername);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbLongCamConnectionString);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 150);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(248, 111);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Long Side Camera";
            // 
            // tbLongCamPassword
            // 
            this.tbLongCamPassword.Location = new System.Drawing.Point(94, 76);
            this.tbLongCamPassword.Name = "tbLongCamPassword";
            this.tbLongCamPassword.PasswordChar = '*';
            this.tbLongCamPassword.Size = new System.Drawing.Size(141, 20);
            this.tbLongCamPassword.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Password:";
            // 
            // tbLongCamUsername
            // 
            this.tbLongCamUsername.Location = new System.Drawing.Point(94, 50);
            this.tbLongCamUsername.Name = "tbLongCamUsername";
            this.tbLongCamUsername.Size = new System.Drawing.Size(141, 20);
            this.tbLongCamUsername.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Username:";
            // 
            // tbLongCamConnectionString
            // 
            this.tbLongCamConnectionString.Location = new System.Drawing.Point(94, 24);
            this.tbLongCamConnectionString.Name = "tbLongCamConnectionString";
            this.tbLongCamConnectionString.Size = new System.Drawing.Size(141, 20);
            this.tbLongCamConnectionString.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "IP Address:";
            // 
            // vspPreview
            // 
            this.vspPreview.Location = new System.Drawing.Point(266, 150);
            this.vspPreview.Name = "vspPreview";
            this.vspPreview.Size = new System.Drawing.Size(295, 137);
            this.vspPreview.TabIndex = 12;
            this.vspPreview.Text = "videoSourcePlayer1";
            this.vspPreview.VideoSource = null;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.btnCopySS);
            this.groupBox2.Controls.Add(this.btnCopyLS);
            this.groupBox2.Controls.Add(this.btnPreview);
            this.groupBox2.Controls.Add(this.lstCameras);
            this.groupBox2.Location = new System.Drawing.Point(267, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(294, 131);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Camera List";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(171, 12);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(117, 23);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnCopySS
            // 
            this.btnCopySS.Location = new System.Drawing.Point(171, 70);
            this.btnCopySS.Name = "btnCopySS";
            this.btnCopySS.Size = new System.Drawing.Size(117, 23);
            this.btnCopySS.TabIndex = 16;
            this.btnCopySS.Text = "Copy to Short Side";
            this.btnCopySS.UseVisualStyleBackColor = true;
            this.btnCopySS.Click += new System.EventHandler(this.btnCopySS_Click);
            // 
            // btnCopyLS
            // 
            this.btnCopyLS.Location = new System.Drawing.Point(171, 99);
            this.btnCopyLS.Name = "btnCopyLS";
            this.btnCopyLS.Size = new System.Drawing.Size(117, 23);
            this.btnCopyLS.TabIndex = 15;
            this.btnCopyLS.Text = "Copy to Long Side";
            this.btnCopyLS.UseVisualStyleBackColor = true;
            this.btnCopyLS.Click += new System.EventHandler(this.btnCopyLS_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(171, 41);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(117, 23);
            this.btnPreview.TabIndex = 14;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // lstCameras
            // 
            this.lstCameras.FormattingEnabled = true;
            this.lstCameras.Location = new System.Drawing.Point(6, 17);
            this.lstCameras.Name = "lstCameras";
            this.lstCameras.Size = new System.Drawing.Size(159, 108);
            this.lstCameras.TabIndex = 0;
            // 
            // EditTableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 299);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.vspPreview);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bgCameraInformation);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.numTableId);
            this.Controls.Add(this.lblTableNum);
            this.Name = "EditTableForm";
            this.Text = "Table Information";
            ((System.ComponentModel.ISupportInitialize)(this.numTableId)).EndInit();
            this.bgCameraInformation.ResumeLayout(false);
            this.bgCameraInformation.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTableNum;
        private System.Windows.Forms.NumericUpDown numTableId;
        private System.Windows.Forms.Label lblConnectionString;
        private System.Windows.Forms.TextBox tbShortCamConnectionString;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbShortCamUsername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbShortCamPassword;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox bgCameraInformation;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbLongCamPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbLongCamUsername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbLongCamConnectionString;
        private System.Windows.Forms.Label label5;
        private AForge.Controls.VideoSourcePlayer vspPreview;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnCopySS;
        private System.Windows.Forms.Button btnCopyLS;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.ListBox lstCameras;
    }
}