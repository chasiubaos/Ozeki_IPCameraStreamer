﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Facebook;

namespace SportGameRecorder
{
    public partial class FacebookLoginForm : Form
    {
        private const string _appId = "450491692077077";
        private Uri _loginUrl;
        private const string _permissions = "publish_actions,publish_pages,manage_pages";

        public FacebookClient _fbClient;
        public FacebookOAuthResult _facebookOAuthResult { get; private set; }

        public FacebookLoginForm(FacebookClient client) 
        {
            InitializeComponent();
            _fbClient = client;
        }

        private void FacebookLogin_Load(object sender, EventArgs e)
        {
            Login();
        }

        private void Login()
        {
            dynamic parameters = new ExpandoObject();
            parameters.client_id = _appId;
            parameters.redirect_uri = "https://www.facebook.com/connect/login_success.html";
            parameters.response_type = "token";
            parameters.display = "popup";
            parameters.scope = _permissions;

            _fbClient = new FacebookClient();
            _loginUrl = _fbClient.GetLoginUrl(parameters);

            browser.Navigate(_loginUrl.AbsoluteUri);
        }

        private void browser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            FacebookOAuthResult oAuthResult;
            if (_fbClient.TryParseOAuthCallbackUrl(e.Url, out oAuthResult))
            {
                _facebookOAuthResult = oAuthResult;
                _fbClient.AccessToken = oAuthResult.AccessToken;
                DialogResult = _facebookOAuthResult.IsSuccess ? DialogResult.OK : DialogResult.No;
            }
            else
            {
                _facebookOAuthResult = null;
            }
        }
    }
}
