﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ozeki.Camera;
using Ozeki.Media;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SportGameRecorder.Forms
{
    public partial class EditTableForm : Form
    {
        public List<Table> currentTables;
        public int? tableIndex = null;

        private IIPCamera _ipCamera;
        private VideoViewerWF _videoViewerWf;
        private BitmapSourceProvider _provider;
        private MediaConnector _connector;

        public EditTableForm(List<Table> tables, int id) : this(tables)
        {
            // In case we need to change the table number, we should store the table num in the beginning
            tableIndex = id;

            // Populate the form with the appropriate values for this table
            Table table = (Table)tables.Find(x => x.id == tableIndex);
            numTableId.Value = table.id;
            tbShortCamConnectionString.Text = table.shortCam.connectionString;
            tbShortCamUsername.Text = table.shortCam.username;
            tbShortCamPassword.Text = table.shortCam.password;
            tbLongCamConnectionString.Text = table.longCam.connectionString;
            tbLongCamUsername.Text = table.longCam.username;
            tbLongCamPassword.Text = table.shortCam.password;
        }

        public EditTableForm(List<Table> tables)
        {
            InitializeComponent();
            currentTables = tables;

            _connector = new MediaConnector();
            _provider = new BitmapSourceProvider();

            _videoViewerWf = new VideoViewerWF();
            _videoViewerWf.Size = new Size(vspPreview.Width, vspPreview.Height);
            _videoViewerWf.BackColor = Color.Black;
            _videoViewerWf.TabStop = false;
            _videoViewerWf.Location = new Point(10, 20);
            _videoViewerWf.Name = "Preview";

            vspPreview.Controls.Add(_videoViewerWf);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Camera shortCam = new Camera(
                tbShortCamConnectionString.Text,
                tbShortCamUsername.Text,
                tbShortCamPassword.Text);
            Camera longCam = new Camera(
                tbLongCamConnectionString.Text,
                tbLongCamUsername.Text,
                tbLongCamPassword.Text);
            Table table = new Table(Decimal.ToInt16(numTableId.Value), shortCam, longCam);
            
            if (!currentTables.Exists(x => x.id == table.id) || tableIndex == table.id)
            {
                if (tableIndex == null)
                {
                    currentTables.Add(table);
                    currentTables = currentTables.OrderBy(t => t.id).ToList();
                }
                else
                {
                    int index = currentTables.FindIndex(x => x.id == tableIndex);
                    currentTables[index] = table;
                }

                // Convert table object to json string
                JArray json = (JArray)JToken.FromObject(currentTables);

                File.WriteAllText("./tables.json", json.ToString());

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Table already exists.", "Error", MessageBoxButtons.OK);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            lstCameras.Items.Clear();

            //Make sure that only one event is assigned
            IPCameraFactory.DeviceDiscovered -= IPCameraFactory_DeviceDiscovered;
            IPCameraFactory.DeviceDiscovered += IPCameraFactory_DeviceDiscovered;

            IPCameraFactory.DiscoverDevices();
        }

        private void IPCameraFactory_DeviceDiscovered(object sender, DiscoveryEventArgs e)
        {
            new Task(() =>
            {
                lstCameras.Items.Add(e.Device.Host + ":" + e.Device.Port);
            }).Start();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            new Task(() =>
            {

            });
        }

        private void btnCopySS_Click(object sender, EventArgs e)
        {
            if (lstCameras.SelectedIndex >= 0)
            {
                tbShortCamConnectionString.Text = lstCameras.SelectedItem.ToString();
            }
        }

        private void btnCopyLS_Click(object sender, EventArgs e)
        {
            if (lstCameras.SelectedIndex >= 0)
            {
                tbLongCamConnectionString.Text = lstCameras.SelectedItem.ToString();
            }
        }
    }
}
