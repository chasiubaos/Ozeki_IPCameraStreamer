﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportGameRecorder
{
    public class Camera
    {
        public String connectionString { get; set; }
        public String username { get; set; }
        public String password { get; set; }

        public Camera(String connectionString, String username, String password)
        {
            this.connectionString = connectionString;
            this.username = username;
            this.password = password;
        }
    }
}
