﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportGameRecorder
{
    public class Table
    {
        public int id { get; set; }
        public Camera shortCam { get; set; }
        public Camera longCam { get; set; }

        public Table(int id, Camera shortCamera, Camera longCamera)
        {
            this.id = id;
            this.shortCam = shortCamera;
            this.longCam = longCamera;
        }
    }
}
